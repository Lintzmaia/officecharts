Main scene location:
Assets/Scenes/MainScene.unity



Scripts located on Assets/Scripts.

DataHolder: Holdes the employee name and Chart Data.

MouseScript: Controls the mouse Input.

RayControl: Controls the gaze input.

SetupScene: Place employee on vacant Work Station.



To generate PC compatible Build:

1) On MainScene, go to HeadHolder GameObject.
2) Disable "Left Eye" children.
3) On "Right Eye" children, go to inspector and disable "Cardboard Head" script and "Ray Control" script.
4) Enable "Mouse Script" script.
5) Under "Camera" component of "Right Eye", set the Viewport Rect value of W to 1.



To generate a Cardboard compatible build:

1) On MainScene, go to HeadHolder GameObject.
2) Enable "Left Eye" children.
3) On "Right Eye" children, go to inspector and disable "Mouse Script" script.
4) Enable "Ray Control" script and "Cardboard Head" script.
5) Under "Camera" component of "Right Eye", set the Viewport Rect value of w to 0.5.


To Change Office space:

1) Have the Office Scene in place, generally assets from unity Asset Store come with a demo scene, where all the prefabs, e.g. tables, lamps, cabinets, ect. are placed in the scene.
2) Open this scene and place in it the HeadHolder Prefab and the SceneManeger prefabs, located at  
Assets/Prefabs. Place by dragging it the Hierarchy tab or the Scene tab.
3) Move the HeadHolder instance where you want the camera to be positioned, you can rotate it as well.



To Change the Employee Prefab model:

1) Place the new model in a scene, any scene, and click on the Model GameObject, in the Hierarchy tab.
2) In the Inspector tab, Click "Add Component" and go to Scripts/ DataHolder, to add the Class that holds the data.
3) Still on the Inspector tab, click again in "Add Component" and go to Physics/Capsule Collider.
4) On the Capsule Collider that was just added, you can adjust the height, Center and Radius, so it cover all the new employee model.
5) Still on the Inspector tab, Change the Tag and Layer to "Employee", Both Tag and Layer setting are just bellow the GameObject name, on the top of the Inspector tab.
6) Drag the GameObject, from the Hierarchy tab to a folder in the Project tab, I recommend the Assets/Prefabs folder, but any would do.

!!! This is the new prefab you should use when Adding a new employee!!!
!!! If the employee does not fit the chair, by being too high or too low, you can change the "Local Position" Vector3, located on SceneManeger GameObject, to make position Adjustments to the position!!!



To add new Camera Position Chair:

1) Add, on the scene, the prefab "CameraChair", located on Assets/Prefab.
2) Move the chair in the desired location.
!!!To add the prefab to the scene you need to drag the prefab icon to the Hierarchy tab or the Scene tab!!!



To add employee:

1) Drag and drop "Employee Prefab", located at Assets/Prefabs
2) Click on the instantiated employee, and on the Inspector tab set employee name and chart data on the "Data Holder" script.



To Set a Viable Work Station:

1) Find a chair or instantiate one and set its tag to "WorkStation".
!!!The ideal chair for the employee prefab position is "PREF_chair_01", located under Assets/SimpleOffice/Prefabs!!!.



Mouse Script class Configs:

You can set witch button to press to drag the camera, by default set to mouse 1.
Checking the Invert Camera Axis box will invert the camera controls.
M Sensitivity controls the mouse sensitivity.



Ray Control class Configs:

Time To Call New Chart sets for how long the user gaze has to stay on the employee before calling the chart of the employee.
Time To Call Chair Change set for how long the user gaze has to stay on the blue chair before changing the camera position.
