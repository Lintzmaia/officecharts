﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CiclularProgress : MonoBehaviour {	
	bool IsRunning;
	LineChart lineChart;
	public GameObject LineChartFill;
	public Text EmployeeNameBox;
	GameObject GazinAt;
	Material NeutralState;
	public Material[] EmployeeStates;

	void Awake(){
		lineChart = LineChartFill.GetComponentInChildren<LineChart> ();
		gameObject.GetComponent<Renderer>().material.SetFloat("_Progress", -1);
	}

	public void RunProgress (float timeToComplete, GameObject GazeAt) {
		if (GazinAt != null) {
			GazinAt.GetComponent<Renderer> ().material = NeutralState;
		}
	//	NeutralState = GazeAt.GetComponent<Renderer> ().material;
		GazinAt = GazeAt;
	//	GazeAt.GetComponent<Renderer> ().material = EmployeeStates [1];
		if (!IsRunning) {
			StartCoroutine (RadialProgress (timeToComplete, GazeAt));
		}
	}
	public void ResetProgress(){
		StopAllCoroutines ();
		IsRunning = false;
		gameObject.GetComponent<Renderer> ().material.SetFloat ("_Progress", -1);
		if (GazinAt != null){
		//	GazinAt.GetComponent<Renderer> ().material = NeutralState;
			NeutralState = null;
			GazinAt = null;
		}
	}

	IEnumerator RadialProgress(float time, GameObject GazeAt)
	{
		IsRunning = true;
		float rate = 1 / time;
		float i = 0;
		while (i < 1)
		{
			i += Time.deltaTime * rate;
			gameObject.GetComponent<Renderer>().material.SetFloat("_Progress", i);
			yield return 0;
		}
		switch (GazeAt.layer) {
		case 8:
			transform.parent.parent.parent.position = GazeAt.transform.position;
			transform.parent.parent.parent.rotation = GazeAt.transform.rotation;
			break;
		case 9:
			LineChartFill.SetActive (true);
			DataHolder Data = (DataHolder)GazeAt.GetComponent<DataHolder> ();
			//GazeAt.GetComponent<Renderer> ().material = EmployeeStates [2];

			lineChart.UpdateData (Data.ChartData);
			EmployeeNameBox.text = Data.EmployeeName;
			gameObject.GetComponent<Renderer> ().material.SetFloat ("_Progress", -1);
			break;
		}
		IsRunning = false;
	}
}