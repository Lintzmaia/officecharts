﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RayControl : MonoBehaviour {
	
	public LayerMask ChairLayer;
	public LayerMask EmployeeLayer;
	public float TimeToCallChairChange;
	public float TimeToCallNewChart;
	public Text HighLightText;

	Camera Cam;
	CiclularProgress ReticuleLoding;
	public LayerMask ChartLayer;
	public Material Neutral;
	public Material HighLightMaterial;
	void Start () {
		ReticuleLoding = (CiclularProgress)FindObjectOfType<CiclularProgress> ();
		Cam = GetComponent<Camera> ();
	}
	RaycastHit Hit;
	RaycastHit CurHit;
	void Update () {
		Ray Check = Cam.ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0));
		if (!Physics.Raycast (Check, out Hit, 100, ChartLayer)) {
			if (Physics.Raycast (Check, out Hit, 100, EmployeeLayer)) {
				if (Hit.collider != CurHit.collider && CurHit.collider != null) {
					if (CurHit.collider.tag != "Employee") {
						CurHit.collider.gameObject.GetComponent<Renderer> ().material = Neutral;
					}
					ResetLoading ();
				}
				SentReference = "Employee";

				PassLoadingReference (Hit.collider.gameObject);

			} else if (Physics.Raycast (Check, out Hit, 100, ChairLayer)) {
				if (Hit.collider != CurHit.collider && CurHit.collider != null) {
					if (CurHit.collider.tag != "Employee") {
						CurHit.collider.gameObject.GetComponent<Renderer> ().material = Neutral;
					}
					ResetLoading ();
				}
				Hit.collider.gameObject.GetComponent<Renderer> ().material = HighLightMaterial;
				SentReference = "Chair";
				PassLoadingReference (Hit.collider.gameObject);
			} else {
				if (CurHit.collider != null && CurHit.collider.tag != "Employee") {
					CurHit.collider.gameObject.GetComponent<Renderer> ().material = Neutral;
				}
				ResetLoading ();
			}
			CurHit = Hit;
		}

	}

	void ResetLoading(){
		ReticuleLoding.ResetProgress ();
		HighLightText.text = null;
		SentReference = null;
		CurSentReference = null;
	}


	string CurSentReference;
	string SentReference;
	public void PassLoadingReference(GameObject Reference){
		if (SentReference == null) {
			
			switch (SentReference) {
			case "Chair":
				ReticuleLoding.RunProgress (TimeToCallChairChange, Reference);
				break;
			case "Employee":
				ReticuleLoding.RunProgress (TimeToCallNewChart, Reference);
				break;
				
			}
		}else if (SentReference != CurSentReference) {
			if (Reference.tag == "Employee") {
				HighLightText.text = Reference.GetComponent<DataHolder> ().EmployeeName;
			}
			ReticuleLoding.ResetProgress ();
			ReticuleLoding.RunProgress (TimeToCallChairChange, Reference);


		}
		CurSentReference = SentReference;
	}


}
