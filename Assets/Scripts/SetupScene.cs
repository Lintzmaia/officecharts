﻿using UnityEngine;
using System.Collections;

public class SetupScene : MonoBehaviour {
	public static SetupScene Setup;
	public Vector3 LocalPosition = new Vector3 (0.66f, 0, -0.55f);
	void Awake () {
		if (Setup == null) {
			Setup = this;
			DontDestroyOnLoad (this.gameObject);
		} else if (Setup != this) {
			GameObject.Destroy (this.gameObject);
		}
	}

	void Start(){
		GameObject[] Workstation = GameObject.FindGameObjectsWithTag ("WorkStation");
		GameObject[] Employee = GameObject.FindGameObjectsWithTag ("Employee");

		for (int pass = 0; pass < Employee.Length; pass++) {
			Employee [pass].transform.position = LocalPosition;
			Employee [pass].transform.rotation = Quaternion.Euler (Vector3.zero);
			Employee [pass].transform.SetParent (Workstation [pass].transform, false);		
		}
	}
}
