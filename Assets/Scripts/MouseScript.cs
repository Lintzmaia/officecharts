﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MouseScript : MonoBehaviour {
	Camera Cam;

	public GameObject LineChartFill;
	LineChart lineChart;
	public Text EmployeeNameBox;
	public Text NameHighLight;

	public LayerMask EmployeeLayer;

	public LayerMask ChairLayer;

	public Material[] HighlightMaterials;
	public Material Neutral;
	public KeyCode ButtonToDrag;

	public bool InvertCameraAxis;
	public float mSensitivity;


	void Start () {
		lineChart = LineChartFill.GetComponentInChildren<LineChart> ();
		Cam = GetComponent<Camera> ();

	}
	int CheckInversionValue(){
		if (InvertCameraAxis) {
			return -1;
		}
		return 1;
	}

	RaycastHit Hit;
	GameObject CurHighlight;

	void Update () {
		
		RayCheck ();

		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			if (CurHighlight != null) {
				if (CurHighlight.layer == 9) {
					DataHolder Data = CurHighlight.GetComponent<DataHolder> ();
					lineChart.UpdateData (Data.ChartData);
					EmployeeNameBox.text = Data.EmployeeName;
					LineChartFill.SetActive (true);
				} else if (CurHighlight.layer == 8) {
					transform.parent.position = CurHighlight.transform.position;
					transform.parent.rotation = CurHighlight.transform.rotation;
				}
			}
		}
		if (Input.GetKey (ButtonToDrag)) {
			
			Vector3 mVec = new Vector3 (-Input.GetAxis ("Mouse Y"), Input.GetAxis ("Mouse X"), 0) * mSensitivity * CheckInversionValue ();
			Vector3 newRotation = transform.rotation.eulerAngles + mVec;
			transform.rotation = Quaternion.Euler(newRotation);
		}


	}



	public void RayCheck(){
		Ray Check = Cam.ScreenPointToRay (Input.mousePosition);
		if (Physics.Raycast (Check, out Hit, 100, EmployeeLayer)) {

			if (CurHighlight != Hit.collider.gameObject && CurHighlight != null) {
				if (CurHighlight.tag != "Employee") {
					CurHighlight.GetComponent<Renderer> ().material = Neutral;
				}
				CurHighlight = null;
				NameHighLight.text = null;
			}
			if (CurHighlight == null) {
				CurHighlight = Hit.collider.gameObject;
			
				NameHighLight.text = CurHighlight.GetComponent<DataHolder> ().EmployeeName;

			}
			return;
		} else {
			if (CurHighlight != null) {
				if (CurHighlight.tag != "Employee") {
					CurHighlight.GetComponent<Renderer> ().material = Neutral;
					
				}
				CurHighlight = null;
				NameHighLight.text = null;
			}

		}

		if (Physics.Raycast (Check, out Hit, 1000, ChairLayer)) {

			if (CurHighlight != Hit.collider.gameObject && CurHighlight != null) {
				CurHighlight.GetComponent<Renderer> ().material = Neutral;

				CurHighlight = null;
			}

			if (CurHighlight == null) {
				CurHighlight = Hit.collider.gameObject;


				CurHighlight.GetComponent<Renderer> ().material = HighlightMaterials [0];

			}
		} else {
			if (CurHighlight != null && CurHighlight.tag != "Employee") {
				CurHighlight.GetComponent<Renderer> ().material = Neutral;
				CurHighlight = null;
				NameHighLight.text = null;
			}
			NameHighLight.text = null;
		}
	}




}
